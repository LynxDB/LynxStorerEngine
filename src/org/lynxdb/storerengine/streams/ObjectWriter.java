/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.lynxdb.storerengine.streams;

import com.google.gson.*;
import com.google.gson.stream.JsonWriter;
import org.lynxdb.lynxlist.structure.LynxList;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;

import static java.nio.file.StandardOpenOption.TRUNCATE_EXISTING;

/**
 *
 * @author martin
 */
public class ObjectWriter<T> {
    private final Gson gson;
    private final File fileObjects;
    private final Class<T> objectClass;

    private final String JSONSEPARATOR = ",\n  ";

    public ObjectWriter(File fileObjects, Class<T> objectClass) throws IOException {
        this.gson = new GsonBuilder().setPrettyPrinting().create();
        this.fileObjects = fileObjects;
        this.objectClass = objectClass;
    }
    
    public ObjectWriter(String fileObjectsPath, Class<T> objectClass)
            throws IOException{
        this(new File(fileObjectsPath), objectClass);
    }

    private void updateJsonContent(JsonArray array) throws IOException {
        JsonWriter writer = new JsonWriter(new FileWriter(fileObjects));
        writer.jsonValue(gson.toJson(array));
        writer.close();
    }

    public void writeObject(T obj) throws IOException {
        FileReader fileReader = new FileReader(fileObjects);
        JsonParser parser = new JsonParser();
        try {
            JsonArray array = parser.parse(fileReader).getAsJsonArray();
            array.add(parser.parse(gson.toJson(obj)));
            updateJsonContent(array);
        } catch (IllegalStateException e) {
            Files.write(fileObjects.toPath(), gson.toJson(obj, objectClass).getBytes(), TRUNCATE_EXISTING);
        } finally {
            fileReader.close();
        }
    }

    public void writeObjectsFrom(LynxList<T> listObjects) throws IOException{
        FileReader fileReader = new FileReader(fileObjects);
        JsonParser parser = new JsonParser();
        try {
            JsonArray array = parser.parse(fileReader).getAsJsonArray();

            for (T obj : listObjects)
                array.add(parser.parse(gson.toJson(obj)));

            updateJsonContent(array);
        } catch (IllegalStateException e) {
            Files.write(fileObjects.toPath(), gson.toJson(listObjects,
                    listObjects.getClass()).getBytes(), TRUNCATE_EXISTING);
        } finally {
            fileReader.close();
        }
    }
    
    public void clearFile() throws IOException{
        fileObjects.createNewFile();
    }

    public void update(LynxList<T> listObjects) throws IOException {
        Files.write(fileObjects.toPath(),
                gson.toJson(listObjects, listObjects.getClass()).getBytes(),
                TRUNCATE_EXISTING);
    }

    /*public static void main(String[] args) {
        JsonParser parser = new JsonParser();
        JsonElement parse = parser.parse(new FileReader(null));
        JsonArray jsonArray = parse.getAsJsonArray();
        JsonWriter writer = new JsonWriter(null);
        jsonArray.toString();
    }*/



}
