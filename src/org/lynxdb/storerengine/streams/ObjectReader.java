/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.lynxdb.storerengine.streams;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import org.lynxdb.lynxlist.structure.LynxList;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Type;

/**
 *
 * @author martin
 * @param <T>
 */
public class ObjectReader<T> {
    private final Gson gson;
    private final File fileObjects;
    private Class<T> objectClass;

    public ObjectReader(File fileObjects, Class<T> objectClass)
            throws IOException {
        this.gson = new GsonBuilder().setPrettyPrinting().create();
        this.fileObjects = fileObjects;
        this.objectClass = objectClass;
        if (!fileObjects.exists())
            fileObjects.createNewFile();
    }
    
    public ObjectReader(String fileObjectsPath, Class<T> objectClass) throws IOException {
        this(new File(fileObjectsPath), objectClass);
    }

    /*private String getFileContent() throws IOException {
        return new String(Files.readAllBytes(fileObjects.toPath()));
    }*/

    public boolean hasData() throws IOException {
        /*if (count == 0 && fileObjects.length() == 0)
            Files.write(fileObjects.toPath(), "{}".getBytes(),
                    StandardOpenOption.TRUNCATE_EXISTING);*/
        return getObjectCount() > 0;
    }

    public int getObjectCount() throws IOException {
        FileReader fileReader = new FileReader(fileObjects);
        int count = 0;
        try {
            count = new JsonParser().parse(fileReader)
                    .getAsJsonArray().size();
        } catch (IllegalStateException e) {
        } finally {
            fileReader.close();
        }
        return count;
    }

    public T readObject(int index) throws IOException {
        FileReader fileReader = new FileReader(fileObjects);
        JsonParser parser = new JsonParser();

        try {
            JsonElement element = parser.parse(fileReader);
            JsonElement firstElement = element.getAsJsonArray().get(index);
            Type type = new TypeToken<T>(){}.getType();
            return gson.fromJson(firstElement, type);
        } catch (IllegalStateException e) {
            return null;
        } finally {
            fileReader.close();
        }
    }

    public T readFirstObject() throws IOException {
        return readObject(0);
    }

    public LynxList<T> readAllObjects() throws IOException {
        LynxList<T> listObjects = new LynxList<>();
        FileReader fileReader = new FileReader(fileObjects);
        JsonParser parser = new JsonParser();

        try {
            JsonArray array = parser.parse(fileReader).getAsJsonArray();
            Type type = new TypeToken<T>(){}.getType();

            for (int i = 0; i < array.size(); i++)
               listObjects.add(gson.fromJson(array.get(i), type));

        } catch (IllegalStateException e) {
        } finally {
            fileReader.close();
        }
        return listObjects;
    }

    /*public static void main(String[] args) throws IOException {
        File file = new File("xd.db");
        file.createNewFile();
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        LinkedList<Persona> list = new LinkedList<>();

        for (int i = 0; i < 10; i++)
            list.add(new Persona(i, "name"+i));
        Files.write(file.toPath(), gson.toJson(list,
                list.getClass()).getBytes(), StandardOpenOption.TRUNCATE_EXISTING);

        FileReader fileReader = new FileReader(file);
        JsonParser parser = new JsonParser();
        JsonElement element = parser.parse(fileReader);
        JsonArray array = element.getAsJsonArray();

        System.out.println("Size: "+array.size());
        JsonElement martin = parser.parse(gson.toJson(new Persona(21, "martin")));
        array.add(martin);
        System.out.println("Size: "+array.size());
        System.out.println(file.getCanonicalPath());

        JsonWriter writer = new JsonWriter(new FileWriter(file));
        writer.jsonValue(gson.toJson(array));
        writer.close();

    }*/

}
