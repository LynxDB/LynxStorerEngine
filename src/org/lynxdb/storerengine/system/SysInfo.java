/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.lynxdb.storerengine.system;

import java.io.File;
import java.io.IOException;

/**
 *
 * @author martin
 */
public class SysInfo {
    public static final String OS_NAME = System.getProperty("os.name").toLowerCase();
    public static final String USER_NAME = System.getProperty("user.name");
    private static final boolean IS_WINDOWS = OS_NAME.contains("windows");
    public static final File ROOT_DIR;
    
    static {
        //String rootPath = IS_WINDOWS ? "C:/Users/"+USER_NAME+"/lynxdb" : "/home/"+USER_NAME
        //        +"/"+"lynxdb";
        String rootPath = "lynxdb";
        ROOT_DIR = new File(rootPath);
        if (!ROOT_DIR.exists()) ROOT_DIR.mkdir();
    }

    public static File getTableDir(String dbName, String tableName) throws IOException {
        StringBuilder sb = new StringBuilder();
        sb.append(SysInfo.ROOT_DIR.getCanonicalPath());
        sb.append(File.pathSeparatorChar);
        sb.append(dbName);
        sb.append(File.pathSeparatorChar);
        sb.append(tableName);
        sb.append(File.pathSeparatorChar);
        return new File(sb.toString());
    }
    
}
