/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.lynxdb.storerengine.store.threads;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.lynxdb.lynxlist.structure.LynxList;
import org.lynxdb.storerengine.streams.ObjectWriter;

/**
 *
 * @author martin
 */
public class TSaver<T> extends Thread{
    private final LynxList<T> listObjects;
    private final ObjectWriter<T> writer;
    private int currentObjectCount;
    private int listSize;
    private boolean save;
    
    public TSaver(LynxList<T> listObjects, ObjectWriter<T> writer) {
        this.listObjects = listObjects;
        this.writer = writer;
        currentObjectCount = listSize = listObjects.size();
        setName("TSaver "+getId());
    }

    public boolean hasNewObjects(){
        return currentObjectCount < listObjects.size();
    }
    
    public void updateCounter(){
        listSize = currentObjectCount = listObjects.size();
    }
    
    public void saveNow(){
        save = true;
    }
    
    @Override
    public void run() {
        LynxList<T> subList;
        while (true) {            
            try {
                if (hasNewObjects()) {
                    //listSize = listObjects.size();
                    subList = (LynxList<T>)
                            listObjects.subList(currentObjectCount, listObjects.size());
                    updateCounter();
                    for (T t : subList) {
                        writer.writeObject(t);
                    }
                }
                Thread.sleep(10);
            } catch (InterruptedException | IOException ex) {
                Logger.getLogger(TSaver.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
}
